# Welcome to PolyMath!

PolyMath is both a high-level language and and embedded Python language for compilation on heterogenous hardware.

### Requirements

PolyMath requires Python 3.6 or greater. All third party libraries can be installed using the instructions below.  

### Installation instructions

To install PolyMath, run the following command:

```bash
pip3 install -r requirements.txt
```

In order to be able to run the Lenet-5 Demo, you will also need to install TVM by following the instructions on their documentation located ![here](https://docs.tvm.ai/install/index.html).


### Running PolyMath Demos

There are two PolyMath demos located in the root directory:

1. `memory_enhance.py`: This is an implementation of the "Memory Enhance" application described in Section 2, paragraph 2 of the PolyMath paper.
                        In addition to constructing the _mg_-DFG representing this aglorithm, there is a function for printing out the node names and operation types
                        for each node of the _mg_-DFG, with indentations to represent the varying levels of granularity. The following command will run this demo:
                        `python3 memory_enhance_example.py`

2. `lenet.py`: This is an implementation of the Lenet-5 Neural Network. In addition to constructing the _mg_-DFG representing this aglorithm,
               this program executes inference for a single input on Lenet-5, and validates the result against a Numpy implementation of Lenet-5.
               In addition, this program demonstrates translation to TVM's Relay IR by running a series of compiler passes on the PolyMath _mg_-DFG.
               To validate the result, Lenet-5 is generated using a manual implementation of TVM. The IR produced by both PolyMath and TVM are printed at the end of the program.
                The following command will run this demo: `python3 lenet5.py`
