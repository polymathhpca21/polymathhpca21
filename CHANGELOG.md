# Changelog

All notable changes to this project will be documented in this file.

## [1.0] - 2020-08-20

### Changed
- Fixed `README.md` to include proper installation instructions, as one of the file names changed
- Fixed issue with `setup.py` installation instruction and added missing dependency to `requirements.txt` file
