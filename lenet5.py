# Make sure TVM is installed prior to running this file!

import pickle
import polymath as pm
import tvm
import numpy as np
from itertools import product
from tests.util import conv3d, np_relu, pooling, batch_flatten, np_softmax

# Numpy implementation of lenet for validation against PolyMath implementation
def np_lenet(lowered=False):
    input_info = {}
    input_info["data"] = np.random.randint(0, 5, (1, 1, 32, 32))
    input_info["w1"] = np.random.randint(0, 5, (6, 1, 5, 5))
    input_info["b1"] = np.random.randint(0, 5, 6)
    input_info["s1"] = 1
    input_info["p1"] = 0

    input_info["w2"] = np.random.randint(0, 5, (16, 6, 5, 5))
    input_info["b2"] = np.random.randint(0, 5, 16)
    input_info["s2"] = 1
    input_info["p2"] = 0

    input_info["w6"] = np.random.randint(0, 5, (120, 400))
    input_info["w7"] = np.random.randint(0, 5, (84, 120))
    input_info["w8"] = np.random.randint(0, 5, (10, 84))
    out_info = {}
    c1_params = {"stride": input_info["s1"], "pad": input_info["p1"]}
    out_info["c1"] = conv3d(input_info["data"], input_info["w1"], input_info["b1"], c1_params)[0]
    out_info["a1"] = np_relu(out_info["c1"])
    out_info["l1"] = pooling(out_info["a1"], 2, 2, 0, 2)

    c2_params = {"stride": input_info["s2"], "pad": input_info["p2"]}
    out_info["c2"] = conv3d(out_info["l1"], input_info["w2"], input_info["b2"], c2_params)[0]
    out_info["a2"] = np_relu(out_info["c2"])
    out_info["l2"] = pooling(out_info["a2"], 2, 2, 0, 2)

    out_info["f5"] = batch_flatten(out_info["l2"])
    out_info["f6"] = input_info["w6"].dot(out_info["f5"])
    out_info["a6"] = np_relu(out_info["f6"])

    out_info["f7"] = input_info["w7"].dot(out_info["a6"])
    out_info["a7"] = np_relu(out_info["f7"])

    out_info["f8"] = input_info["w8"].dot(out_info["a7"])
    out_info["a8"] = np_relu(out_info["f8"])
    out_info["sm"] = np_softmax(out_info["a8"])
    if lowered:
        for k in list(input_info.keys()):
            if hasattr(input_info[k], "shape"):
                pairs = list(product(*tuple([np.arange(i) for i in input_info[k].shape])))
                for p in pairs:
                    input_info[f"{k}/{k}{p}"] = input_info[k][p]
                input_info.pop(k)
        key = []
        for k in list(out_info.keys()):
            if hasattr(out_info[k], "shape"):
                pairs = list(product(*tuple([np.arange(i) for i in out_info[k].shape])))

                for p in pairs:
                    if k == "sm":
                        key.append(f"{k}/{k}{p}")
                    out_info[f"{k}/{k}{p}"] = out_info[k][p]

                if k != "sm":
                    out_info.pop(k)
    else:
        key = "sm"
    return input_info, key, out_info

def tvm_lenet(num_classes=10, data_shape=(1, 1, 32, 32),
              dtype='float32', alpha=1.0, is_shallow=False):
    from tvm import relay
    from tvm.relay.testing import layers

    """Function to construct a Lenet"""
    data = relay.var("data", shape=data_shape, dtype=dtype)
    conv1 = layers.conv2d(data=data, channels=6, kernel_size=(5, 5), name='conv1')
    conv1 = relay.nn.relu(conv1)
    pool2 = relay.nn.avg_pool2d(conv1, pool_size=(2, 2), strides=(2, 2))
    conv3 = layers.conv2d(data=pool2, channels=16, kernel_size=(5, 5), name='conv3')
    conv3 = relay.nn.relu(conv3)
    pool4 = relay.nn.avg_pool2d(conv3, pool_size=(2, 2), strides=(2, 2))
    flattened5 = relay.nn.batch_flatten(pool4)

    fcw5 = relay.var('fc5_weight')
    fc5 = relay.nn.dense(data=flattened5, weight=fcw5, units=120)
    fc5 = relay.nn.relu(fc5)

    fcw6 = relay.var('fc6_weight')
    fc6 = relay.nn.dense(data=fc5, weight=fcw6, units=84)
    fc6 = relay.nn.relu(fc6)

    fcw7 = relay.var('fc7_weight')
    fc7 = relay.nn.dense(data=fc6, weight=fcw7, units=num_classes)
    fc7 = relay.nn.relu(fc7)

    softmax = relay.nn.softmax(data=fc7)
    fn = relay.Function(relay.analysis.free_vars(softmax), softmax)
    return fn

def lenet():
    with pm.Node(name="lenet") as graph:
        n = pm.parameter(name="n")
        c = pm.parameter(name="ic")
        ih = pm.parameter(name="ih")
        iw = pm.parameter(name="iw")
        nf1 = pm.parameter(name="nf1")
        kh1 = pm.parameter(name="kh1")
        kw1 = pm.parameter(name="kw1")
        data = pm.input(name="data", shape=(n, c, ih, iw))
        w1 = pm.state(name="w1", shape=(nf1, c, kh1, kw1))
        b1 = pm.state(name="b1", shape=(nf1))

        s1 = pm.parameter(name="s1")
        p1 = pm.parameter(name="p1")
        c1 = pm.output(name="c1")
        a1 = pm.output(name="a1")
        l1 = pm.output(name="l1")

        pm.conv(data, w1, b1, c1, s1, p1)
        pm.relu(c1, a1)
        pm.avg_pool2d(a1, l1, 2, 2, 2, 0)

        nf2 = pm.parameter(name="nf2")
        kh2 = pm.parameter(name="kh2")
        kw2 = pm.parameter(name="kw2")
        w2 = pm.state(name="w2", shape=(nf2, nf1, kh2, kw2))
        b2 = pm.state(name="b2", shape=(nf2))
        s2 = pm.parameter(name="s2")
        p2 = pm.parameter(name="p2")
        c2 = pm.output(name="c2")
        a2 = pm.output(name="a2")
        l2 = pm.output(name="l2")

        pm.conv(l1, w2, b2, c2, s2, p2)
        pm.relu(c2, a2)
        pm.avg_pool2d(a2, l2, 2, 2, 2, 0)

        f5 = pm.output(name="f5")
        pm.batch_flatten(l2, f5)

        f6 = pm.output(name="f6")
        m6 = pm.parameter(name="m6")
        n6 = pm.parameter(name="n6")
        w6 = pm.state(name="w6", shape=(n6, m6))
        a6 = pm.output(name="a6")
        pm.dense(f5, w6, f6)
        pm.relu1d(f6, a6)

        f7 = pm.output(name="f7")
        m7 = pm.parameter(name="m7")
        n7 = pm.parameter(name="n7")
        w7 = pm.state(name="w7", shape=(n7, m7))
        a7 = pm.output(name="a7")
        pm.dense(a6, w7, f7)
        pm.relu1d(f7, a7)

        f8 = pm.output(name="f8")
        m8 = pm.parameter(name="m8")
        n8 = pm.parameter(name="n8")
        w8 = pm.state(name="w8", shape=(n8, m8))
        a8 = pm.output(name="a8")
        pm.dense(a7, w8, f8)
        pm.relu1d(f8, a8)

        out = pm.output(name="sm")
        pm.softmax(a8, out)
    in_info, keys, out_info = np_lenet()
    return graph, in_info, out_info, keys

def main():
    graph, inp_info, out_info, key = lenet()
    coarse_cpy = pickle.loads(pickle.dumps(inp_info))
    print("Evaluating Lenet-5 Using PolyMath...")
    res = graph(key, coarse_cpy)
    np.testing.assert_allclose(res, out_info[key])
    tvm_code = pm.generate_tvm(graph, inp_info, "")
    pm_mod = tvm.IRModule.from_expr(tvm_code)
    pm_mod = tvm.relay.transform.InferType()(pm_mod)

    net = tvm_lenet()
    mod = tvm.IRModule.from_expr(net)
    mod = tvm.relay.transform.InferType()(mod)

    print(f"------------------------------------TVM Output-----------------------")
    print(mod)

    print(f"-------------------------PolyMath-Compiled TVM Output-----------------")
    print(pm_mod)


if __name__ == "__main__":
    main()